<?php


namespace Ucc\Services;


use JsonMapper;
use KHerGe\JSON\JSON;
use Ucc\Models\Question;

class QuestionService
{
    const QUESTIONS_PATH = __DIR__ . '/../../questions.json';
    const QUESTIONS_SOCCER_PATH = __DIR__ . '/../../questions-soccer.json';

    private JSON $json;
    private JsonMapper $jsonMapper;

    public function __construct(JSON $json, JsonMapper $jsonMapper)
    {
        $this->json = $json;
        $this->jsonMapper = $jsonMapper;
    }

    public function getRandomQuestions(int $count = 5): array
    {
        //TODO: Get {$count} random questions from JSON

        //TODO move the questions paths to a ENUM class
        switch ($count) {
            case '1':
                $poll = $this->json->decodeFile(self::QUESTIONS_PATH);
                break;
            
            case '2':
                $poll = $this->json->decodeFile(self::QUESTIONS_SOCCER_PATH);
                break;                
            
            default:
                $poll = $this->json->decodeFile(self::QUESTIONS_PATH);
                break;
        }

        $question = $poll[rand(0, count($poll) - 1)];
        $questionModel = $this->jsonMapper->map($question, new Question());
        return $questionModel->jsonSerialize();
    }

    public function getPointsForAnswer(int $id, string $answer): int
    {
        $questionModel = null;
        $poll = $this->json->decodeFile(self::QUESTIONS_PATH);
        foreach ($poll as $value) {
            if ($value->id === $id) {
                $questionModel = $this->jsonMapper->map($value, new Question());
                break;
            }
        }

        if (strcmp($questionModel->getCorrectAnswer(), $answer) === 0) {
            return $questionModel->getPoints();
        }
        return 0;

    }
}