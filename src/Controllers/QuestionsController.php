<?php


namespace Ucc\Controllers;


use Ucc\Session;
use Ucc\Http\JsonResponseTrait;
use Ucc\Services\QuestionService;

class QuestionsController extends Controller
{
    use JsonResponseTrait;
    private QuestionService $questionService;

    public function __construct(QuestionService $questionService)
    {
        parent::__construct();
        $this->questionService = $questionService;
    }

    public function beginGame(): bool
    {
        $name = $this->requestBody->name ?? null;
        if (empty($name)) {
            return $this->json('You must provide a name', 400);
        }

        Session::set('name', $name);
        Session::set('questionCount', 1);
        Session::set('points', 0);
        //TODO Get first question for user
        $question = null;

        //TODO I create a trivia category picking we need to move this
        $trivia = $this->requestBody->trivia ?? 1;

        $question = $this->questionService->getRandomQuestions($trivia);
        $questionId = $question['id'];
        Session::set('currentQuestion', $questionId);
        return    $this->json(['question' => $question], 201);
    }

    public function answerQuestion(int $id): bool {
        if ( Session::get('name') === null ) {
            return $this->json('You must first begin a game', 400);
        }

        //TODO implement a validation class
        if (Session::get('currentQuestion') != $id) {
            return $this->json('This is not the question!', 400);
        }

        if ((int)Session::get('questionCount') > 4) {
            $name = Session::get('name');
            $points = Session::get('points');
            Session::destroy();
            return $this->json(['message' => "Thank you for playing {$name}. Your total score was: {$points} points!"]);
        }

        $answer = $this->requestBody->answer ?? null;
        if (empty($answer)) {
            return $this->json('You must provide an answer', 400);
        }

        //TODO: Check answer and increment user's points. Reply with a proper message
        $message = '';
        $question = null;        
        $pointFromAnswer = $this->questionService->getPointsForAnswer($id, $answer);
        Session::set('points', (Session::get('points') + $pointFromAnswer));
        $points = Session::get('points');
        $left = 5 - Session::get('questionCount');
        if ($pointFromAnswer > 0) {
            Session::set('questionCount', (Session::get('questionCount') + 1));
            $left = 5 - Session::get('questionCount');
            $message = "The answer is correct, your current score is {$points}pts you are still missing {$left} questions.";
        } else {
            $message = "The answer is incorrect, your current score is {$points}pts you are still missing {$left} questions.";
        }

        $question = $this->questionService->getRandomQuestions();
        $questionId = $question['id'];
        Session::set('currentQuestion', $questionId);
        return $this->json(['message' => $message, 'question' => $question]);
    }
}